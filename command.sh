#/bin/bash

if [! -f "vendor/autoload.php"]; then
    composer install --no-progress --no-interaction
fi

if [! -f ".env"]; then
    echo "create .env file for this app"
    cp .evn.example .env
else
    echo ".env file already exists"
fi

bun install
bun run build
php artisan key:generate
php artisan migrate
php artisan cache:clear
php artisan config:clear
php artisan route:clear

